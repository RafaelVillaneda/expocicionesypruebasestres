'''
Created on 23 nov. 2020

@author: Rafael Villaneda
'''
from random import *
from ast import Num
import time

class MetodosOrdenamientos:
    def __init__(self):
        self.contador=[0,0,0]
    def mostrarContador(self):
        #1-> recorridos 2-> intercambios 3-> comparaciones
        print(f"Numeros de recorridos: {self.contador[0]}")
        print(f"Numeros de intercambios: {self.contador[1]}")
        print(f"Numeros de Comparaciones: {self.contador[2]}")
        self.contador[0]=0
        self.contador[1]=0
        self.contador[2]=0
    def  ordenacionBurbuja1(self,arreglo):
        for i in range(1, len(arreglo)):
            for j in range(0, len(arreglo)-i):
                self.contador[2]=self.contador[2]+1
                if arreglo[j]>arreglo[j+1]:
                    aux = arreglo[j]
                    self.contador[1]= self.contador[1]+1
                    arreglo[j] = arreglo[j+1]
                    self.contador[1]= self.contador[1]+1
                    arreglo[j+1] = aux
                self.contador[0]=self.contador[0]+1
            self.contador[0]=self.contador[0]+1
    def  ordenacionBurbuja2(self,arreglo):
        i = 0
        while i<len(arreglo):
            j=i
            while j<len(arreglo):
                if arreglo[i]>arreglo[j]:
                    aux = arreglo[i]
                    arreglo[i] = arreglo[j]
                    arreglo[j] = aux
                j+=1
            i+=1
    def  ordenacionBurbuja3(self,arreglo):
        cont = 1
        inicio = time.time()
        while cont<len(arreglo):
            self.contador[0]=self.contador[0]+1
            for j in range(0, len(arreglo)-cont):
                self.contador[0]=self.contador[0]+1
                if arreglo[j]>arreglo[j+1]:
                    self.contador[2]=self.contador[2]+1
                    self.contador[1]=self.contador[1]+1
                    aux = arreglo[j]
                    arreglo[j] = arreglo[j+1]
                    self.contador[1]=self.contador[1]+1
                    arreglo[j+1] = aux
            cont+=1
    
    def ordenarInsercion(self,numeros):
        aux=0
        for i in range(1,len(numeros)):
            aux=numeros[i]
            j=(i-1)
            while(j>=0 and numeros[j]>aux):
                self.contador[2]=self.contador[2]+1
                self.contador[1]=self.contador[1]+1
                numeros[j+1]=numeros[j]
                self.contador[1]=self.contador[1]+1
                numeros[j]=aux
                j-=1
                self.contador[0]=self.contador[0]+1
            self.contador[0]=self.contador[0]+1
    def ordenarSeleccion(self,numeros):
        #1-> recorridos 2-> intercambios 3-> comparaciones
        for i in range(0,len(numeros)):
            for j in range(i,len(numeros)):
                self.contador[2]=self.contador[2]+1
                if(numeros[i]>numeros[j]):
                    minimo=numeros[i]
                    self.contador[1]=self.contador[1]+1
                    numeros[i]=numeros[j]
                    self.contador[1]=self.contador[1]+1
                    numeros[j]=minimo
                self.contador[0]=self.contador[0]+1
            self.contador[0]=self.contador[0]+1
        
    def ordenarQuickSort(self,numeros,izq,der):
        pivote=numeros[izq]
        i=izq
        j=der
        aux=0
        while i < j:
            while numeros[i]<= pivote and i<j:
                i+=1
                self.contador[0]=self.contador[0]+1
            while numeros[j]>pivote:
                j-=1
                self.contador[0]=self.contador[0]+1
            if(i<j):
                aux=numeros[i]
                self.contador[1]=self.contador[1]+1
                numeros[i]=numeros[j]
                self.contador[1]=self.contador[1]+1
                numeros[j]=aux
            self.contador[0]=self.contador[0]+1
        self.contador[1]=self.contador[1]+1
        numeros[izq]=numeros[j]
        self.contador[1]=self.contador[1]+1
        numeros[j]=pivote
        self.contador[2]=self.contador[2]+1
        if izq<j-1:
            self.ordenarQuickSort(numeros, izq, j-1)
        self.contador[2]=self.contador[2]+1
        if j+1<der:
            self.ordenarQuickSort(numeros, j+1, der)

    def ordenarShellsort(self,numeros):
        intervalo=len(numeros)/2
        intervalo=int(intervalo)
        while(intervalo>0):
            for i in range(int(intervalo),len(numeros)):
                j=i-int(intervalo)
                while(j>=0):
                    k=j+int(intervalo)
                    self.contador[2]=self.contador[2]+1
                    if(numeros[j]<=numeros[k]):
                        j=-1
                    else:
                        aux=numeros[j]
                        self.contador[1]=self.contador[1]+1
                        numeros[j]=numeros[k]
                        self.contador[1]=self.contador[1]+1
                        numeros[k]=aux
                        j-=int(intervalo)
                    self.contador[0]=self.contador[0]+1
                self.contador[0]=self.contador[0]+1
            intervalo=int(intervalo)/2
            self.contador[0]=self.contador[0]+1

    def ordenar(self,numeros,exp1):
        n=len(numeros)
        cantidadDatos=[0]*(n)
        contar=[0]*(10)
        
        for i in range(0,n):
            indice=(numeros[i]/exp1)
            contar[int((indice)%10)]+=1
            self.contador[0]=self.contador[0]+1
        for i in range(1,10):
            contar[i]+=contar[i-1]
            self.contador[0]=self.contador[0]+1
        i=n-1
        while i>=0:
            self.contador[2]=self.contador[2]+1
            indice=(numeros[i]/exp1)
            cantidadDatos[contar[int((indice)%10)]-1]=numeros[i]
            contar[int((indice)%10)]-=1
            i-=1
            self.contador[0]=self.contador[0]+1
        i=0
        
        for i in range(0,len(numeros)):
            numeros[i]=cantidadDatos[i]
            self.contador[0]=self.contador[0]+1
    
    def radixSort(self,numeros):
        max1=max(numeros)
        exp=1
        while max1/exp>0:
            self.contador[2]=self.contador[2]+1
            self.ordenar(numeros,exp)
            exp*=10
            self.contador[0]=self.contador[0]+1
      
    def mezclaDirecta(self,array):
        
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
           
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
        
        return array
    
    def mezclaDirecta2(self, array):
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                if(arregloIz[0]< arregloDer[0]):
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
           
            while len(arregloIz)>0:
                array.append(arregloIz.pop(0))
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
        
        
    def mezclaNatural(self, numeros):
        izquerdo = 0
        izq = 0
        derecho = len(numeros)-1
        der = derecho
        ordenado=False
        
        while(not ordenado):
            ordenado = True
            izquierdo =0
            while(izquierdo<derecho):
                izq=izquerdo
                while(izq < derecho and numeros[izq]<=numeros[izq+1]):
                    izq=izq+1
                der=izq+1
                while(der==derecho-1 or der<derecho and numeros[der]<=numeros[der+1]):
                    der=der+1
                if(der<=derecho):
                    self.mezclaDirecta2(numeros)
                    ordenado= False
                izquierdo = izq
              
            
            
        
#Pruebas
vector1000E=[randint(0,1000) for i in range(1000)]
vector10000E=[randint(0,10000) for i in range(10000)]
vector100000E=[randint(0,100000) for i in range(100000)]
ordenar=MetodosOrdenamientos()
op=""
menu=False
vector1=[10,12,5,2,8,3,1,7,22,6,30,4,11,2,9]#Prueba vector predefinido
while(menu==False):
    copi=[]
    op2=""
    bandera=False
    banderaSubmenu=False
    opSubmenu=""
    print("1-> Burbuja")
    print("2- Metodo ordenamiento incercion")
    print("3- Metodo ordenamiento Seleccion")
    print("4- Metodo ordenamiento QUICKSORT")
    print("5- Metodo ordenamiento SHELLSORT")
    print("6- Metodo ordenamiento RADIX")
    print("7- Metodo ordenamiento INTERCALACIÓN")
    print("8- Metodo ordenamiento Mezcla directa")
    print("9- Metodo ordenamiento Mezcla naturala")
    print("10- Metodo busqueda Binaria")
    print("11- Metodo busqueda funciones HASH")
    print("12- Salir")
    op=input("Elije la opcion: ")
    if(op=="1"):
        print("Metodo de ordenamiento burbuja")
        opBurbuja=""
        banderaBurbuja=False
        while(banderaBurbuja==False):
            print("Que quieres ordenar?")
            print("1-> Elementos precargados")
            print("2-> Ordenar 1000 elementos")
            print("3-> ordenar 10000 elementos (ojo Puede llevar un tiempo la prueba)")
            print("4-> Ordenar 100000 elmentos (Ojo algoritmo mas ineficiente tardara demasiado la prueba o simplemente nunca la acabara)")
            opBurbuja=input()
            if(opBurbuja=="1"or opBurbuja=="2" or opBurbuja=="3" or opBurbuja=="4"):
                banderaBurbuja=True
        while(bandera==False):
            print("1-> Metodo de ordenamiento por burbuja 1")
            print("2-> Metodo de ordenamiento por burbuja 2")
            print("3-> Metodo de ordenamiento por burbuja 3")
            op2=input()
            if(op2=="1"):
                print("Burbuja 1")
                if(opBurbuja=="1"):
                    copi=vector1.copy()
                    print("Se ordenaran los elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja1(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    break
                    ordenar.mostrarContador()
                elif(opBurbuja=="2"):
                    copi=vector1000E.copy()
                    print("Se ordenaran 1000 elementos")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja1(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
                elif(opBurbuja=="3"):
                    copi=vector10000E.copy()
                    print("Se ordenaran 10000 elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja1(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
                elif(opBurbuja=="4"):
                    copi=vector100000E.copy()
                    print("Se ordenaran 10000 elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja1(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
                break
            elif(op2=="2"):
                print("Burbuja 2")
                if(opBurbuja=="1"):
                    copi=vector1.copy()
                    print("Se ordenaran los elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja2(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    break
                    ordenar.mostrarContador()
                elif(opBurbuja=="2"):
                    copi=vector1000E.copy()
                    print("Se ordenaran 1000 elementos")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja2(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
                elif(opBurbuja=="3"):
                    copi=vector10000E.copy()
                    print("Se ordenaran 10000 elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja2(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
                elif(opBurbuja=="4"):
                    copi=vector100000E.copy()
                    print("Se ordenaran 10000 elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja2(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
            elif(op2=="3"):
                print("Burbuja 3")
                if(opBurbuja=="1"):
                    copi=vector1.copy()
                    print("Se ordenaran los elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja3(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    break
                    ordenar.mostrarContador()
                elif(opBurbuja=="2"):
                    copi=vector1000E.copy()
                    print("Se ordenaran 1000 elementos")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja3(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
                elif(opBurbuja=="3"):
                    copi=vector10000E.copy()
                    print("Se ordenaran 10000 elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja3(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
                elif(opBurbuja=="4"):
                    copi=vector100000E.copy()
                    print("Se ordenaran 10000 elementos precargados")
                    inicio=int(time.time())
                    ordenar.ordenacionBurbuja3(copi)
                    final=int(time.time())
                    print(f"Se tardo: {(final-inicio)/1000}")
                    ordenar.mostrarContador()
        bandera=False
    elif(op=="2"):
        print("Metodo de ordenamiento por insercion")
        while(banderaSubmenu==False):
            print("Que quieres ordenar?")
            print("1-> Elementos precargados")
            print("2-> Ordenar 1000 elementos")
            print("3-> ordenar 10000 elementos")
            print("4-> Ordenar 100000 elmentos (Ojo puede tardar demasiado la prueba )")
            opSubmenu=input()
            if(opSubmenu=="1"or opSubmenu=="2" or opSubmenu=="3" or opSubmenu=="4"):
                banderaSubmenu=True
        banderaSubmenu=False
        if(opSubmenu=="1"):
            tiempoIn=0
            print("Se ordenaran los elementos precargados")
            tiempoIn=time.time()
            ordenar.ordenarInsercion(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="2"):
            tiempoIn=0
            copi=vector1000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarInsercion(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="3"):
            tiempoIn=0
            copi=vector10000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarInsercion(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="4"):
            tiempoIn=0
            copi=vector100000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarInsercion(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="5"):
            print("Saliendo...")
            banderaSubmenu=True
    elif(op=="3"):
        print("Metodo de ordenamiento por Seleccion")
        while(banderaSubmenu==False):
            print("Que quieres ordenar?")
            print("1-> Elementos precargados")
            print("2-> Ordenar 1000 elementos")
            print("3-> ordenar 10000 elementos")
            print("4-> Ordenar 100000 elmentos (Ojo puede tardar demasiado la prueba )")
            opSubmenu=input()
            if(opSubmenu=="1"or opSubmenu=="2" or opSubmenu=="3" or opSubmenu=="4"):
                banderaSubmenu=True
        banderaSubmenu=False
        if(opSubmenu=="1"):
            tiempoIn=0
            print("Se ordenaran los elementos precargados")
            copi=vector1.copy()
            tiempoIn=time.time()
            ordenar.ordenarSeleccion(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="2"):
            tiempoIn=0
            copi=vector1000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarSeleccion(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="3"):
            tiempoIn=0
            copi=vector10000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarSeleccion(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="4"):
            tiempoIn=0
            copi=vector100000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarSeleccion(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
    elif(op=="4"):
        print("Metodo de ordenamiento por QUICKSORT")
        while(banderaSubmenu==False):
            print("Que quieres ordenar?")
            print("1-> Elementos precargados")
            print("2-> Ordenar 1000 elementos")
            print("3-> ordenar 10000 elementos")
            print("4-> Ordenar 100000 elmentos ")
            opSubmenu=input()
            if(opSubmenu=="1"or opSubmenu=="2" or opSubmenu=="3" or opSubmenu=="4"):
                banderaSubmenu=True
        banderaSubmenu=False
        if(opSubmenu=="1"):
            tiempoIn=0
            print("Se ordenaran los elementos precargados")
            copi=vector1.copy()
            tiempoIn=time.time()
            ordenar.ordenarQuickSort(copi, 0, len(copi)-1)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="2"):
            tiempoIn=0
            copi=vector1000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarQuickSort(copi, 0, len(copi)-1)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="3"):
            tiempoIn=0
            copi=vector10000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarQuickSort(copi, 0, len(copi)-1)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="4"):
            tiempoIn=0
            copi=vector100000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarQuickSort(copi, 0, len(copi)-1)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
    elif(op=="5"):
        print("Metodo de ordenacion por shellsort")
        while(banderaSubmenu==False):
            print("Que quieres ordenar?")
            print("1-> Elementos precargados")
            print("2-> Ordenar 1000 elementos")
            print("3-> ordenar 10000 elementos")
            print("4-> Ordenar 100000 elmentos ")
            opSubmenu=input()
            if(opSubmenu=="1"or opSubmenu=="2" or opSubmenu=="3" or opSubmenu=="4"):
                banderaSubmenu=True
        banderaSubmenu=False
        if(opSubmenu=="1"):
            tiempoIn=0
            print("Se ordenaran los elementos precargados")
            copi=vector1.copy()
            tiempoIn=time.time()
            ordenar.ordenarShellsort(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="2"):
            tiempoIn=0
            copi=vector1000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarShellsort(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="3"):
            tiempoIn=0
            copi=vector10000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarShellsort(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="4"):
            tiempoIn=0
            copi=vector100000E.copy()
            tiempoIn=time.time()
            ordenar.ordenarShellsort(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
    elif(op=="6"):
        print("Metodo de ordenacion por radix")
        while(banderaSubmenu==False):
            print("Que quieres ordenar?")
            print("1-> Elementos precargados")
            print("2-> Ordenar 1000 elementos")
            print("3-> ordenar 10000 elementos")
            print("4-> Ordenar 100000 elmentos (Puede tardar la prueba)")
            opSubmenu=input()
            if(opSubmenu=="1"or opSubmenu=="2" or opSubmenu=="3" or opSubmenu=="4"):
                banderaSubmenu=True
        banderaSubmenu=False
        if(opSubmenu=="1"):
            tiempoIn=0
            print("Se ordenaran los elementos precargados")
            copi=vector1.copy()
            tiempoIn=time.time()
            ordenar.radixSort(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="2"):
            tiempoIn=0
            copi=vector1000E.copy()
            tiempoIn=time.time()
            ordenar.radixSort(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="3"):
            tiempoIn=0
            copi=vector10000E.copy()
            tiempoIn=time.time()
            ordenar.radixSort(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            ordenar.mostrarContador()
        elif(opSubmenu=="4"):
            tiempoIn=0
            copi=vector100000E.copy()
            tiempoIn=time.time()
            ordenar.radixSort(copi)
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
    elif(op=="7"):
        print("Aun no se expone")
    elif(op=="8"):
        print("Aun no se expone")
    elif(op=="9"):
        print("Aun no se expone")
    elif(op=="10"):
        print("Aun no se expone")
    elif(op=="11"):
        print("Aun no se expone")
    elif(op=="12"):
        print("Saliendo....")
        menu=True
    else:
        print("La opcion que seleccionaste no existe")
        


print("Metodo de ordenamiento por mezcla Natural")
numeros = [22,23,30,2,5,10,9,13]
print(f"Desordenado: {numeros}")
ordenar.mezclaNatural(numeros)
print(f"Ordenados: {numeros}")
